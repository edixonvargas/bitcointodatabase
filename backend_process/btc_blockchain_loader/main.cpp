#include <QCoreApplication>
#include <QtSql>
#include <QtNetwork>
#include <QProcess>

//#include <conio.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>

#ifdef _WIN32
    #include <Windows.h>
#elif __unix
    #include <unistd.h>
#endif
//VARIABLES

const int cliTimeout = 600000;

QSqlDatabase db;

QProcess bitcoin_cli;

QSqlQuery query;

QString
	btcCLICommand,
	normalOutput,
	errorOutput;

void signalLog(int signal);
void logStatus(QtMsgType type, const QMessageLogContext &context, const QString &msg);
void updateBlockTransactions(const QJsonObject &block);
bool sendQuery(const QString &statement, QSqlQuery &query);

int main(int argc, char *argv[])
{

	signal(SIGSEGV, signalLog);

	qInstallMessageHandler(logStatus);

	QCoreApplication app(argc, argv);

	app.setApplicationName("blockchainloader");
	app.setApplicationVersion("1.0.0");

	QCommandLineParser *parser = new QCommandLineParser();
	parser->setApplicationDescription("Loads a MySQL Database with blockchaindata");
	parser->addHelpOption();
	parser->addVersionOption();

	QCommandLineOption
			argDB			(QStringList() << "db" << "database",
							 QCoreApplication::translate("main", "The data base to connect to"),
							 "database"),
			argHost			("host",
							 QCoreApplication::translate("main", "The database's host"),
							 "localhost"),
			argPort			(QStringList() << "p" << "port",
							 QCoreApplication::translate("main", "The database's host"),
							 "portnumber",
							 "3306"),
			argUser 		(QStringList() << "u" << "user",
							 QCoreApplication::translate("main", "The database user"),
							 "username"),
			argPassword 	("pass",
							 QCoreApplication::translate("main", "The user's password"),
							 "password"),
			argTable	 	(QStringList() << "t" << "table",
							 QCoreApplication::translate("main", "The table where data will be stored"),
							 "table"),
			argStartFromBlock	(QStringList() << "startfrom" << "block",
								 QCoreApplication::translate("main", "The block from this program will start to index"),
								 "startfrom"),
			argTS 			(QStringList() << "ts" << "training-set",
							 QCoreApplication::translate("main", "The training set to be automatically loaded."),
							 "training-set-file",
							 QCoreApplication::translate("main", "The file path."));

	parser->addOption(argDB);
	parser->addOption(argHost);
	parser->addOption(argPort);
	parser->addOption(argUser);
	parser->addOption(argPassword);
	parser->addOption(argTable);
	parser->addOption(argStartFromBlock);
//	parser->addOption(argTS);

	parser->process(app);

	if(parser->isSet(argDB) && parser->isSet(argHost) && parser->isSet(argUser) && parser->isSet(argPassword))
	{
		int port;
		QString
				database = parser->value(argDB),
				host = parser->value(argHost),
				user = parser->value(argUser),
				pass = parser->value(argPassword);

		if(parser->isSet(argPort))
		{
			port = parser->value(argPort).toInt();
		}else {
			port = 3306;
		}

		db = QSqlDatabase::addDatabase("QMYSQL");
		db.setDatabaseName(database);
		db.setHostName(host);
		db.setUserName(user);
//		db.setPassword(parser->value(argPassword));
		db.setPassword(pass);
		db.setPort(port);

//		if(db.open()) //Tries to open database connection
//		{
		qInfo() << "Sucessfully connected to database: " << parser->value(argDB);
		qInfo() << "Getting the number of blocks...";

#ifdef _WIN32
		btcCLICommand = "C:\\Data\\Bitcoin\\bitcoin-0.14.1-win64\\bin\\bitcoin-cli.exe -rpcuser=################## -rpcpassword=############## -rpcconnect=192.168.1.2 ";
#elif __unix
        btcCLICommand = "/home/edixon/applications/bitcoin/bitcoin-0.15.1/bin/bitcoin-cli.exe -rpcuser=edixon.vargas -rpcpassword=3d1x0n100986 -rpcconnect=192.168.1.2 ";
#endif
		bitcoin_cli.start(btcCLICommand + "getblockcount");
		bitcoin_cli.waitForFinished(cliTimeout);

		normalOutput = (bitcoin_cli.readAll());
		errorOutput = (bitcoin_cli.readAllStandardError());

		bitcoin_cli.closeReadChannel(QProcess::StandardOutput);
		bitcoin_cli.closeReadChannel(QProcess::StandardError);

		if(errorOutput == "")
		{
			int
					startFrom = 0,
					blockCount = normalOutput.toInt();

			QString sqlLastBlock = "select blockindex from blocks order by blockindex desc limit 1";


			if(parser->isSet(argStartFromBlock))
			{
				startFrom = parser->value(argStartFromBlock).toInt();
			}
			else if(sendQuery(sqlLastBlock, query))
			{
				if(query.next()){
					startFrom = query.record().value("blockindex").toInt() + 1;
					query.clear();
				}
			}

			if(startFrom > blockCount)
			{
				qInfo() << "Can not start from block " << QString::number(startFrom) << " due it is out of range. Will start from the last block.";
				startFrom = blockCount;
			}

			qInfo() << "Starting from block" << QString::number(startFrom);
			qInfo() << "Number of blocks: " << QString::number(blockCount);

			QString nextHash = "";

			bitcoin_cli.start(btcCLICommand + "getblockhash " + QString::number(startFrom));
			bitcoin_cli.waitForFinished(cliTimeout);

			normalOutput = QString(bitcoin_cli.readAllStandardOutput()).remove("\r\n");
			errorOutput = QString(bitcoin_cli.readAllStandardError());

			bitcoin_cli.closeReadChannel(QProcess::StandardOutput);
			bitcoin_cli.closeReadChannel(QProcess::StandardError);

			if(errorOutput == "")
			{
				nextHash = normalOutput;
			}else
			{
				qCritical() << "Error retrieving the hash at block: " << QString::number(startFrom) << " cli says: " << errorOutput;
				QCoreApplication::exit();
			}

			int i = startFrom;

			do
			{
				qInfo() << "Reading block: " << QString::number(i);

				if(errorOutput == "")
				{

					bitcoin_cli.start(btcCLICommand + "getblock " + nextHash);
					bitcoin_cli.waitForFinished(cliTimeout);

					errorOutput = QString(bitcoin_cli.readAllStandardError());

					bitcoin_cli.closeReadChannel(QProcess::StandardError);
					if(errorOutput == "")
					{
						QByteArray data = bitcoin_cli.readAllStandardOutput();

						bitcoin_cli.closeReadChannel(QProcess::StandardOutput);

						QJsonParseError *pe = new QJsonParseError();

						QJsonDocument jdoc = QJsonDocument::fromJson(data, pe);

						QJsonObject json(jdoc.object());

						nextHash = json["nextblockhash"].toString();

						i = json["height"].toInt();

						if(nextHash == "")
						{
							nextHash = json["hash"].toString();
						}

						QString strQuery = "INSERT INTO `crypto_predictions`.`blocks`(`blockindex`,`hash`,`next_block_hash`,`previous_block_hash`,`confirmations`,`stripedsize`,`size`,`weight`,`height`,`version`,`versionhex`,`merkleroot`,`unixtime`,`time`,`unixmediantime`,`mediantime`,`nonce`,`bits`,`difficulty`,`chainwork`) " +
										   QString("VALUES (") +
										   QString::number(i) + ",'" +
								json["hash"].toString() + "','" +
								json["nextblockhash"].toString() + "','" +
								json["previousblockhash"].toString() + "'," +
								QString::number(json["confirmations"].toInt()) + "," +
								QString::number(json["strippedsize"].toInt()) + "," +
								QString::number(json["size"].toInt()) + "," +
								QString::number(json["weight"].toInt()) + "," +
								QString::number(i) + "," +
								QString::number(json["version"].toInt()) + ",'" +
								json["versionHex"].toString() + "','" +
								json["merkleroot"].toString() + "'," +
								QString::number(json["time"].toInt()) + ",'" +
								QDateTime::fromTime_t(json["time"].toInt()).toString("yyyy-MM-dd h:m:s") + "'," +
								QString::number(json["mediantime"].toInt()) + ",'" +
								QDateTime::fromTime_t(json["mediantime"].toInt()).toString("yyyy-MM-dd h:m:s") + "'," +
								QString::number(json["nonce"].toDouble()) + ",'" +
								json["bits"].toString() + "'," +
								QString::number(json["difficulty"].toDouble()) + ",'" +
								json["chainwork"].toString() + "')";

						if(sendQuery(strQuery, query))
						{
							if(i > 0)
								updateBlockTransactions(json);

						}
						else
						{
							int error = query.lastError().number();
							if(nextHash == json["hash"].toString())
							{
								qInfo() << "Last block reached. Waiting for a new block to arrive. Sleeping for 10 seconds";

//								db.close();
								query.clear();
								data.clear();
								delete pe;
								//NOTE: This function is windows-only?
#ifdef _WIN32
                                Sleep(10000);
#elif __unix
                                usleep(10000000);
#endif
								continue;
							}

							if(error == 1062)
							{
								qCritical() << "Block " << QString::number(i) << " already exist in database, data will be updated";

								strQuery = "update `crypto_predictions`.`blocks` set " +

										   QString("`blockindex` = " + QString::number(i)) + "," +
										   QString("`next_block_hash` = '" + json["nextblockhash"].toString()) + "'," +
										   QString("`previous_block_hash` = '" + json["previousblockhash"].toString()) + "'," +
										   QString("`confirmations` = " + QString::number(json["confirmations"].toInt())) + "," +
										QString("`stripedsize` = " + QString::number(json["strippedsize"].toInt())) + "," +
										QString("`size` = " + QString::number(json["size"].toInt())) + "," +
										QString("`weight` = " + QString::number(json["weight"].toInt())) + "," +
										QString("`height` = " + QString::number(json["height"].toInt())) + "," +
										QString("`version` = " + QString::number(json["version"].toInt())) + "," +
										QString("`versionhex` = '" + json["versionHex"].toString()) + "'," +
										QString("`merkleroot` = '" + json["merkleroot"].toString()) + "'," +
										QString("`unixtime` = " + QString::number(json["time"].toInt())) + "," +
										QString("`time` = '" + QDateTime::fromTime_t(json["time"].toInt()).toString("yyyy-MM-dd h:m:s")) + "'," +
										QString("`unixmediantime` = " + QString::number(json["mediantime"].toInt())) + "," +
										QString("`mediantime` = '" + QDateTime::fromTime_t(json["mediantime"].toInt()).toString("yyyy-MM-dd h:m:s")) + "'," +
										QString("`nonce` = '" + QString::number(json["nonce"].toDouble())) + "'," +
										QString("`bits` = '" + json["bits"].toString()) + "'," +
										QString("`difficulty` = " + QString::number(json["difficulty"].toDouble())) + "," +
										QString("`chainwork` = '" + json["chainwork"].toString()) + "'" +
										QString(" where `hash` = '" + json["hash"].toString()) + "'";



//								if(!query.exec(strQuery))

								if(sendQuery(strQuery, query))
								{
									if(i > 0)
										updateBlockTransactions(json);

									qInfo() << "Successfully updated block" << QString::number(i);
								}
								else
								{
									qCritical() << "Error trying to update block " << QString::number(i) << " . Message: " << query.lastError().text();
								}
							}
							else
							{
								qCritical() << "Error trying to load block " << QString::number(i) << " . Message: " << query.lastError().text();
							}
						}
//						db.close();
						query.clear();
						data.clear();
						delete pe;
					}
					else
					{
						qCritical() << "Error retriving block data: block[" << QString::number(i) << "] -> " << errorOutput;

						return 0;
	//							QCoreApplication::exit();
					}

				}
				else
				{
					qCritical() << "Error when trying to get block hash: block[" << QString::number(i) << "] -> " << errorOutput;

					return 0;
	//						QCoreApplication::exit();
				}


			}while(true);

	//				qInfo() << "Last hash reached. Exiting";

	//				QCoreApplication::exit();
		}
		else
		{
			qCritical() << "Error when trying to get the total blocks: " << errorOutput;

			return 0;
	//				QCoreApplication::exit();
		}

//		db.close();
//		}
//		else
//		{
//			qInfo() << "Could not connect to database: " << parser->value(argDB) << ". Server says: MySQL Error: " << QString::number(db.lastError().number()) << ": " << db.lastError().text();

//			return 0;
////			QCoreApplication::exit();
//		}
	}else{
		qCritical() << "Invalid argument set passed.";

		return 0;
//		QCoreApplication::exit();
	}


	return 0;
//	return app.exec();
}


void updateBlockTransactions(const QJsonObject &block)
{
	//TODO: Insert all related transactions
	QJsonArray transactions = block["tx"].toArray();

	foreach(QJsonValue tx, transactions)
	{
		QString txhash = tx.toString();
		bitcoin_cli.start(btcCLICommand + "getrawtransaction " + txhash + " 1");
		bitcoin_cli.waitForFinished(cliTimeout);

		errorOutput = QString(bitcoin_cli.readAllStandardError());

		bitcoin_cli.closeReadChannel(QProcess::StandardError);

		if(errorOutput == "")
		{
			QByteArray data = bitcoin_cli.readAllStandardOutput();

			bitcoin_cli.closeReadChannel(QProcess::StandardOutput);

			QJsonParseError *pe = new QJsonParseError();

			QJsonDocument jdoc = QJsonDocument::fromJson(data, pe);

			QJsonObject json(jdoc.object());

			QString
					strTXID = json["txid"].toString(),
					strInstTX =
					QString("INSERT INTO transactions (`txid`,`txhash`,`version`,`size`,`vsize`,`lock_time`,`hex`,`block_hash`,`confirmations`,`unix_time`,`unix_block_time`,`time`,`block_time`) VALUES (") +
					QString("'" + strTXID + "',") +
					QString("'" + json["hash"].toString() + "',") +
					QString(QString::number(json["version"].toInt()) + ",") +
					QString(QString::number(json["size"].toInt()) + ",") +
					QString(QString::number(json["vsize"].toInt()) + ",") +
					QString(QString::number(json["locktime"].toInt()) + ",") +
					QString("'" + json["hex"].toString() + "',") +
					QString("'" + json["blockhash"].toString() + "',") +
					QString(QString::number(json["confirmations"].toInt()) + ",") +
					QString(QString::number(json["time"].toInt()) + ",") +
					QString(QString::number(json["blocktime"].toInt()) + ",'") +
					QDateTime::fromTime_t(json["time"].toInt()).toString("yyyy-MM-dd h:m:s") + "','" +
					QDateTime::fromTime_t(json["blocktime"].toInt()).toString("yyyy-MM-dd h:m:s") + "')";

			if(sendQuery(strInstTX, query))
			{
				qInfo() << "Transaction" << strTXID << "- Successfully stored.";
			}
			else
			{
//				int error = query.lastError().text();

//				QString msg = query.lastError().text();

				if(query.lastError().number() == 1062)
				{
					QString strUpdateTX =
							QString("update transactions set ") +
//							QString("`txid` = " + QString::number(i)) + "," +
//							QString("`txhash` = '" + json["hash"].toString()) + "'," +
							QString(" `version` = " + QString::number(json["version"].toInt()) + ",") +
							QString(" `size` = " + QString::number(json["size"].toInt())) + "," +
							QString(" `vsize` = " + QString::number(json["vsize"].toInt())) + "," +
							QString(" `lock_time` = " + QString::number(json["locktime"].toInt())) + "," +
							QString(" `hex` = '" + json["hex"].toString()) + "'," +
							QString(" `block_hash` = '" + json["blockhash"].toString()) + "'," +
							QString(" `confirmations` = " + QString::number(json["confirmations"].toInt()) + ",") +
							QString(" `unix_time` = " + QString::number(json["time"].toInt()) + ",") +
							QString(" `unix_block_time` = " + QString::number(json["blocktime"].toInt()) + ",") +
							QString(" `time` = '" + QDateTime::fromTime_t(json["time"].toInt()).toString("yyyy-MM-dd h:m:s") + "',") +
							QString(" `block_time` = '" + QDateTime::fromTime_t(json["blocktime"].toInt()).toString("yyyy-MM-dd h:m:s") + "'") +
//							QString(" `last_update` = now() ") +
							QString(" where txid = '" + strTXID + "'");

					if(sendQuery(strUpdateTX, query))
					{
						qInfo() << "Transaction" << strTXID << "already exists, it was updated.";
					}
					else
					{
						qCritical() << "Could not update transaction" << strTXID << "- Server says: Error[" << QString::number(query.lastError().number()) << "] -> Message: " << query.lastError().text();
					}

				}
				else
				{
					qCritical() << "Error when trying to store transaction" << strTXID << "- Server says: Error[" + QString::number(query.lastError().number()) + "] -> Message: " + query.lastError().text();
				}

			}

//									db.close();
			data.clear();
			query.clear();
			delete pe;
		}
		else
		{
			qCritical() << "Error when getting data of transaction:" << txhash;
		}

	}

	//TODO: should validate if final transaction count is equal to the transaction count of the block

}

bool tryOpenDB(int tries = 10)
{
	int i = 0;
	for(; !db.isOpen() && i < tries; i++)
	{
		if(!db.open())
		{
			qCritical() << "Could not open database. Trying to reconnect after 5 seconds";

#ifdef _WIN32
            Sleep(5000);
#elif __unix
            usleep(5000000);
#endif
		}
		else
		{
//			qInfo() << "Successfully connected to database";

			return true;
		}
	}

	return false;
}

//TODO: Validate if can close database while fetching
bool sendQuery(const QString &statement, QSqlQuery &query)
{
	if(tryOpenDB())
	{

		query = QSqlQuery(db);

		bool q = query.exec(statement);
//		int err = query.lastError().number();
//		QString msg = query.lastError().text();
		db.close();

		return q;
	}
	db.close();
	return false;
}

void logStatus(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
//	QFileInfo fi("./messages.log");
//	QTextStream out;

	QFile file("./messages.log");

	if(file.open(QFile::Append))
	{
		//Open stream file writes
		QTextStream out(&file);
		QString str;

		// Write the date of recording
		str = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");

		// By type determine to what level belongs message
		switch (type)
		{
			case QtInfoMsg:     str += "INF "; break;
			case QtDebugMsg:    str += "DBG "; break;
			case QtWarningMsg:  str += "WRN "; break;
			case QtCriticalMsg: str += "CRT "; break;
			case QtFatalMsg:    str += "FTL "; break;
		}
		// Write to the output category of the message and the message itself
		str += QString(context.category) + ": " + msg + "\r\n";
		std::cout << str.toStdString() << std::flush;
		out << str;
//		<< out.string()->toStdString();
		out.flush();
		file.close();
	}

}

void signalLog(int signal)
{
	switch(signal)
	{
		case SIGINT:			// interrupt
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGINT: interrupt");
			break;
		case SIGILL:			// illegal instruction - invalid function image
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGILL: illegal instruction - invalid function image");
			break;
		case SIGFPE:			// floating point exception
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGFPE: floating point exception");
			break;
		case SIGSEGV:			// segment violation
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGSEGV: segment violation");
			break;
		case SIGTERM:			// Software termination signal from kill
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGTERM: Software termination signal from kill");
			break;
#ifdef _WIN32
		case SIGBREAK:			// Ctrl-Break sequence
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGBREAK: Ctrl-Break sequence");
			break;
#endif
		case SIGABRT:			// abnormal termination triggered by abort call
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGABRT: abnormal termination triggered by abort call");
			break;
	}
}
