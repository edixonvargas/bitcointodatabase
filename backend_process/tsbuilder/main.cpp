#include <QtCore>
#include <QtSql>
#include <QtNetwork>

#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>


void logStatus(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
//	QFileInfo fi("./messages.log");
//	QTextStream out;

	QFile file("./messages.log");

	if(file.open(QFile::Append))
	{
		//Open stream file writes
		QTextStream out(&file);
		QString str;

		// Write the date of recording
		str = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");

		// By type determine to what level belongs message
		switch (type)
		{
			case QtInfoMsg:     str += "INF "; break;
			case QtDebugMsg:    str += "DBG "; break;
			case QtWarningMsg:  str += "WRN "; break;
			case QtCriticalMsg: str += "CRT "; break;
			case QtFatalMsg:    str += "FTL "; break;
		}
		// Write to the output category of the message and the message itself
		str += QString(context.category) + ": " + msg + "\r\n";
		std::cout << str.toStdString();
		out << str;
//		<< out.string()->toStdString();
		file.close();
	}

}

void signalLog(int signal)
{
	switch(signal)
	{
		case SIGINT:			// interrupt
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGINT: interrupt");
			break;
		case SIGILL:			// illegal instruction - invalid function image
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGILL: illegal instruction - invalid function image");
			break;
		case SIGFPE:			// floating point exception
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGFPE: floating point exception");
			break;
		case SIGSEGV:			// segment violation
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGSEGV: segment violation");
			break;
		case SIGTERM:			// Software termination signal from kill
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGTERM: Software termination signal from kill");
			break;
#ifdef _WIN32
        case SIGBREAK:			// Ctrl-Break sequence
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGBREAK: Ctrl-Break sequence");
			break;
#endif
		case SIGABRT:			// abnormal termination triggered by abort call
			logStatus(QtMsgType::QtFatalMsg, QMessageLogContext(), "SIGABRT: abnormal termination triggered by abort call");
			break;
	}
}

int main(int argc, char *argv[])
{
//	struct sigaction sa;

	signal(SIGSEGV, signalLog);

	qInstallMessageHandler(logStatus);

	QCoreApplication app(argc, argv);

	app.setApplicationName("tsbuilder");
	app.setApplicationVersion("1.0.0");

	QCommandLineParser *parser = new QCommandLineParser();
	parser->setApplicationDescription("This tool automatically builds a training set upon a MySQL table");
	parser->addHelpOption();
	parser->addVersionOption();

	QCommandLineOption
			argDB			(QStringList() << "db" << "database",
							 QCoreApplication::translate("main", "The data base to connect to"),
							 "database"),
			argHost			("host",
							 QCoreApplication::translate("main", "The database's host"),
							 "localhost"),
			argPort			(QStringList() << "p" << "port",
							 QCoreApplication::translate("main", "The database's host"),
							 "portnumber",
							 "3306"),
			argUser 		(QStringList() << "u" << "user",
							 QCoreApplication::translate("main", "The database user"),
							 "username"),
			argPassword 	("pass",
							 QCoreApplication::translate("main", "The user's password"),
							 "password"),
			argTable	 	(QStringList() << "t" << "table",
							 QCoreApplication::translate("main", "The table where data will be stored"),
							 "table"),
			argTS 			(QStringList() << "ts" << "training-set",
							 QCoreApplication::translate("main", "The training set to be automatically loaded."),
							 "training-set-file",
							 QCoreApplication::translate("main", "The file path."));

	parser->addOption(argDB);
	parser->addOption(argHost);
	parser->addOption(argPort);
	parser->addOption(argUser);
	parser->addOption(argPassword);
	parser->addOption(argTable);
//	parser->addOption(argTS);

	parser->process(app);

	if(parser->isSet(argDB) && parser->isSet(argHost) && parser->isSet(argUser) && parser->isSet(argPassword))
	{
		int port;
		QString
				database = parser->value(argDB),
				host = parser->value(argHost),
				user = parser->value(argUser),
				pass = parser->value(argPassword);

		if(parser->isSet(argPort))
		{
			port = parser->value(argPort).toInt();
		}else {
			port = 3306;
		}

		QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
		db.setDatabaseName(database);
		db.setHostName(host);
		db.setUserName(user);
//		db.setPassword(parser->value(argPassword));
		db.setPassword(pass);
		db.setPort(port);

		if(db.open()) //Tries to open database connection
		{
			qInfo() << "Sucessfully connected to database: " + parser->value(argDB);

			QString url = "https://api.blockchain.info/charts/market-price?timespan=all";

			QNetworkAccessManager *nam = new QNetworkAccessManager();

			QNetworkReply *reply = nam->get(QNetworkRequest(QUrl(url)));

			QObject::connect(reply, &QNetworkReply::finished, [=]{
				qInfo() << "Getting chart data from " + url;

//				QString data();

//				data = data.replace("\n", "");
				QByteArray data = reply->readAll();

				QJsonParseError *pe = new QJsonParseError();

				QJsonDocument jdoc = QJsonDocument::fromJson(data, pe);
//				qDebug() << jdoc.toBinaryData();
				QJsonObject json(jdoc.object());

				qDebug() << jdoc.isEmpty();

				QJsonArray jarray = json["values"].toArray();

				QSqlQuery query(db);

				for(int i = 30; i < jarray.count(); i++)
				{

					if(query.exec("INSERT INTO `crypto_predictions`.`ts_btc_30days_daily` (`date`, `unix_time`, `input0`, `input1`, `input2`, `input3`, `input4`, `input5`, `input6`, `input7`, `input8`, `input9`, `input10`, `input11`, `input12`, `input13`, `input14`, `input15`, `input16`, `input17`, `input18`, `input19`, `input20`, `input21`, `input22`, `input23`, `input24`, `input25`, `input26`, `input27`, `input28`, `input29`, `target0`) " +
								  QString("VALUES ") +
								  "('" + QDateTime::fromTime_t(jarray[i].toObject()["x"].toInt()).toString("yyyy-MM-dd") + "'," +
								  "" + QString::number(jarray[i].toObject()["x"].toInt()) + ","
								  "" + QString::number(jarray[i - 30].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 29].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 28].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 27].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 26].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 25].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 24].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 23].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 22].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 21].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 20].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 19].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 18].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 17].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 16].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 15].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 14].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 13].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 12].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 11].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 10].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 9].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 8].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 7].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 6].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 5].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 4].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 3].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 2].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i - 1].toObject()["y"].toDouble()) + "," +
								  "" + QString::number(jarray[i].toObject()["y"].toDouble()) + ")"))

					{
						qInfo() << "Day " + QDateTime::fromTime_t(jarray[i].toObject()["x"].toInt()).toString("yyyy-MM-dd") + "has been inserted";
					}
					else
					{
						qWarning() << "Could not insert day " + QDateTime::fromTime_t(jarray[i].toObject()["x"].toInt()).toString("yyyy-MM-dd");
					}
				}
				qInfo() << "All data inserted";


//				db.close();
				QCoreApplication::exit();
			});
			//When data is ready to read
//			QObject::connect(reply, &QNetworkReply::readyRead, [=]{

//			});

			QObject::connect(nam, &QNetworkAccessManager::finished, [=](QNetworkReply *rep){

				switch(rep->error())
				{
					case QNetworkReply::NoError:
						qInfo() << "Sucessfully connected to " + url;
						break;
					case QNetworkReply::ConnectionRefusedError:
					case QNetworkReply::RemoteHostClosedError:
					case QNetworkReply::HostNotFoundError:
					case QNetworkReply::TimeoutError:
					case QNetworkReply::OperationCanceledError:
					case QNetworkReply::SslHandshakeFailedError:
					case QNetworkReply::TemporaryNetworkFailureError:
					case QNetworkReply::NetworkSessionFailedError:
					case QNetworkReply::BackgroundRequestNotAllowedError:
					case QNetworkReply::TooManyRedirectsError:
					case QNetworkReply::InsecureRedirectError:
					case QNetworkReply::UnknownNetworkError:
					case QNetworkReply::ProxyConnectionRefusedError:
					case QNetworkReply::ProxyConnectionClosedError:
					case QNetworkReply::ProxyNotFoundError:
					case QNetworkReply::ProxyTimeoutError:
					case QNetworkReply::ProxyAuthenticationRequiredError:
					case QNetworkReply::UnknownProxyError:
					case QNetworkReply::ContentAccessDenied:
					case QNetworkReply::ContentOperationNotPermittedError:
					case QNetworkReply::ContentNotFoundError:
					case QNetworkReply::AuthenticationRequiredError:
					case QNetworkReply::ContentReSendError:
					case QNetworkReply::ContentConflictError:
					case QNetworkReply::ContentGoneError:
					case QNetworkReply::UnknownContentError:
					case QNetworkReply::ProtocolUnknownError:
					case QNetworkReply::ProtocolInvalidOperationError:
					case QNetworkReply::ProtocolFailure:
					case QNetworkReply::InternalServerError:
					case QNetworkReply::OperationNotImplementedError:
					case QNetworkReply::ServiceUnavailableError:
					case QNetworkReply::UnknownServerError:
						qCritical() << "Could not connect to " + url + ". Error code is: " + QString::number(rep->error()) + "(" + rep->error() + ") " + rep->errorString();
						break;

				}
			});

		}
		else
		{
			qInfo() << "Could not connect to database: " + parser->value(argDB) + ". Server says: MySQL Error: " + QString::number(db.lastError().number()) + ": " + db.lastError().text();
		}
	}else{
		qCritical() << "Invalid argument set passed.";
	}

//	QStringList argList = cmdParser->optionNames();


//	return app.exec();
	return app.exec();
}

