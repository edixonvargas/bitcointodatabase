# INTRO #

BitcoinToDatabase is a simple application that allows you to synchronize a blockchain data inside your database.
To do this, it is neccessary to have bitcoin-core correctly installed and the binaries has to be installed into the PATH variable.


### What we hope? ###

The purpose of this applications is to allow you to have an easy and common way to get the blockchain data in your database.
Because this is the first version it only supports MySQL as database but the future goal is to allow storing in all common databases.

Version 1.0.1


### How do I get set up? ###

This first version has been developed and tested under Windows 64bits only. And you have to have installed the Bitcoin Core
bitcoin-cli.exe file into the path "C:\\Data\\Bitcoin\\bitcoin-0.14.1-win64\\bin\

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact